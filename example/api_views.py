from django.http import JsonResponse
from .models import Blog, Comment
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods



class CommentListEncoder(ModelEncoder):
    model = Comment
    properties = ["content"]

class BlogListEncoder(ModelEncoder):
    model = Blog
    properties = ["title"]

class BlogDetailEncoder(ModelEncoder):
    model = Blog
    properties = ["title", "content",]

@require_http_methods(["GET", "POST"])
def list_blogs(request):
    if request.method == "GET":
        # Get all of the blogs
        blogs = Blog.objects.all()
        # Return the blogs in a JsonResponse
        return JsonResponse({"blogs": blogs}, encoder=BlogListEncoder)
    else:
        content = json.loads(request.body)
        blog = Blog.objects.create(**content)  # ** allows us to get the key value pairs
        return JsonResponse({"blog": blog}, encoder=BlogListEncoder)
        
            

@require_http_methods(["GET", "PUT", "DELETE"])
def show_blog_detail(request, pk):
    if request.method == "GET":
        # Get the blog
        blog = Blog.objects.get(pk=pk)
        # Return the JsonResponse
        return JsonResponse({"blog": blog}, encoder=BlogDetailEncoder)
    elif request.method == "DELETE":
        count, _ = Blog.objects.filter(id=pk).delete()  #count is storing the last expression value, returns specific item referenced in the api/blogs/pk <-
        # .delete() returns a tuple
        return JsonResponse({"delete": count > 0})
    else:
        # convert the JSON PUT request to a dictionary
        content = json.loads(request.body)
        # use that dictionary to update our blog 
        Blog.objects.filter(id=pk).update(**content) # passing content
        # get the updated blog 
        blog = Blog.objects.get(id=pk)
        # return the updated blog
        return JsonResponse({"blog": blog}, encoder=BlogDetailEncoder)

@require_http_methods(["PUT"])
def create_comment(request, blog_id):
    if request.method == "POST":
        blog = Blog.objects.get(id=blog_id)
        content = json.loads(request.body)
        Comment.objects.create(
            content=content["comment"],
            blog=blog,
        )
        return JsonResponse({"message": "created"})
